# Lymey Guild Website

| Production                                                                                                                                         | Development                                                                                                                                          |
| :------------------------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------------------------: |
| [![pipeline status](https://gitlab.com/lymey-guild/website/badges/master/pipeline.svg)](https://gitlab.com/lymey-guild/website/commits/master) | [![pipeline status](https://gitlab.com/lymey-guild/website/badges/develop/pipeline.svg)](https://gitlab.com/lymey-guild/website/commits/develop) |

## Welcome!

We're a group of D&amp;D hobbyists who live in the Duchy of Lymey.

We go on adventures...
We have fun...
We eat...
We drink... a lot.

We'll have more here soon!
