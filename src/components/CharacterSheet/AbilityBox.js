import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import withWidth from '@material-ui/core/withWidth'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import { calcAbilityBonus, displayUp } from '../../utils'

const styles = theme => ({
  advanced: {
    [theme.breakpoints.up('md')]: {
      backgroundImage: 'url("images/D&D Stat Box.svg")',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center'
    },
    [theme.breakpoints.down('sm')]: {
      border: 'solid',
      borderWidth: 1,
      borderColor: theme.palette.primary.main
    },
    backgroundColor: theme.palette.background.article,
    textAlign: 'center',
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
    // marginTop: 20,
    maxHeight: 100,
    maxWwidth: 100
  },
  advancedBonus: {
    paddingTop: 5
  },
  advancedScore: {
    paddingTop: 10
  },
  simpleContainer: {
    textAlign: 'center'
  },
  simpleScores: {
    padding: theme.spacing.unit,
    color: 'inherit'
  },
  simpleText: {
    color: 'inherit',
    marginTop: theme.spacing.unit,
    fontWeight: 'bold'
  }
})

const AbilityBox = ({ ability, classes, variant, width }) =>
  variant === 'simple' ? (
    <div className={classes.simpleContainer}>
      <Typography className={classes.simpleText} variant='subtitle1'>
        {ability.text}
      </Typography>
      <Typography className={classes.simpleScores} variant={displayUp('md', width) ? 'body2' : 'body1'}>
        {ability.score} (+{calcAbilityBonus(ability.score)})
      </Typography>
    </div>
  ) : (
    <Paper className={classes.advanced}>
      <Typography variant='subtitle1'>{ability.text}</Typography>
      <Typography className={classes.advancedBonus} variant={displayUp('md', width) ? 'h6' : 'body1'}>
        +{calcAbilityBonus(ability.score)}
      </Typography>
      <Typography className={classes.advancedScore} variant='body1'>
        {ability.score}
      </Typography>
    </Paper>
  )

AbilityBox.propTypes = {
  ability: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  variant: PropTypes.string,
  width: PropTypes.string.isRequired
}

export default withWidth()(withStyles(styles, { withTheme: true })(AbilityBox))
