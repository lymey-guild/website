import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { isEmpty } from 'react-redux-firebase'
import ReactMarkdown from 'react-markdown'
import * as RPG from '../../utils/rpg'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Modal from '@material-ui/core/Modal'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import AbilityBox from './AbilityBox'
import Clear from '@material-ui/icons/Clear'
import Create from '@material-ui/icons/Create'
import ViewQuilt from '@material-ui/icons/ViewQuilt'

const styles = theme => ({
  buttonClose: {
    float: 'left',
    marginTop: theme.spacing.unit
  },
  buttonEdit: {
    float: 'left',
    marginTop: theme.spacing.unit,
    marginLeft: theme.spacing.unit
  },
  buttonLink: {
    float: 'right',
    marginTop: theme.spacing.unit
  },
  charName: {
    color: 'inherit',
    fontFamily: 'Eczar'
  },
  iconLeft: {
    marginRight: theme.spacing.unit
  },
  iconRight: {
    marginLeft: theme.spacing.unit
  },
  modal: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  notes: {
    border: 'solid',
    borderWidth: 1,
    minHeight: 50
  },
  paper: {
    color: theme.palette.dnd.red,
    top: '50%',
    left: '50%',
    margin: 'auto',
    position: 'absolute',
    width: '50%',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    transform: 'translate(-50%, -50%)',
    [theme.breakpoints.down('xs')]: {
      width: '100%'
    }
  },
  topRight: {
    position: 'absolute',
    top: theme.spacing.unit * 2,
    right: theme.spacing.unit * 4
  },
  stat: {
    color: 'inherit'
  },
  italic: {
    fontStyle: 'italic'
  },
  statBar: {
    flexShrink: 0,
    float: 'clear',
    margin: 0,
    padding: 0,
    marginTop: 20
  },
  title: {
    color: 'inherit'
  },
  titlePad: {
    color: 'inherit',
    paddingTop: 10
  },
  underline: {
    borderBottom: '1px solid',
    paddingBottom: 5,
    width: '100%'
  }
})

class CharacterSheet extends PureComponent {
  static propTypes = {
    character: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    handleModalClose: PropTypes.func.isRequired
  }

  render () {
    const { character, classes } = this.props
    const statBlock = (character.abilities || []).map((ability, index) => ({
      score: ability,
      text: RPG.ABILITY_SCORES[index]
    }))

    return (
      <Modal
        className={classes.modal}
        aria-labelledby='character-sheet'
        open={!isEmpty(this.props.character)}
        onClose={this.props.handleModalClose}
      >
        <Paper className={classes.paper}>
          {/* <Grid container direction='row' justify='center' alignItems='flex-start'> */}
          <Grid container alignItems='flex-start' spacing={16}>
            <Typography className={classes.topRight} variant='body1'>
              Player: {character.owner}
            </Typography>

            {/* Column 1 */}
            <Grid container item md={6}>
              <Grid item xs={12}>
                <Typography className={classes.charName} variant='h4'>
                  {character.name}
                </Typography>
                <Typography variant='body1'>
                  Level {character.level} / {character.race} {character.class}
                  {character.martial ? <span className={classes.italic}> (martial)</span> : <Fragment />}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <img src='/images/stat-block-header-bar.svg' alt='' />
              </Grid>

              <Grid item xs>
                <Typography className={classes.stat} variant='body1'>
                  Armor Class: {character.ac}
                </Typography>
                <Typography className={classes.stat} variant='body1'>
                  Hit Points: {character.hp} ({character.level}d{character.hd})
                </Typography>
                <Typography className={classes.stat} variant='body1'>
                  Speed: {character.speed}
                </Typography>
              </Grid>

              <Grid item xs>
                <Typography className={classes.stat} variant='body1'>
                  Initiative: +
                  {RPG.calcInitiative(
                    (character.abilities || [])[RPG.getAbilityIndex('WIS')],
                    character.level,
                    character.martial
                  )}
                </Typography>
                <Typography className={classes.stat} variant='body1'>
                  Proficiency: +{RPG.calcProficiency(character.level)}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <img src='/images/stat-block-header-bar.svg' alt='' />
              </Grid>

              {/* // Build the Ability Score boxes */}
              {statBlock.map(ability => (
                <Grid item xs={2} key={ability.text}>
                  <AbilityBox ability={ability} variant='simple' />
                </Grid>
              ))}

              <Grid item xs={12}>
                <img src='/images/stat-block-header-bar.svg' alt='' />
              </Grid>

              <Grid item xs={4}>
                <Typography className={classes.stat} variant='body1'>
                  Prep: {RPG.calcAbilityBonus((character.abilities || [])[RPG.getAbilityIndex('INT')]) + 1}
                </Typography>
              </Grid>
              <Grid item xs={4}>
                <Typography className={classes.stat} variant='body1'>
                  Budget: {character.level - 2}
                </Typography>
              </Grid>
              <Grid item xs={4}>
                <Typography className={classes.stat} variant='body1'>
                  Wealth: {character.wealth}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <img src='/images/stat-block-header-bar.svg' alt='' />
              </Grid>
            </Grid>

            {/* Column 2 */}
            <Grid container item md={6}>
              <Grid item xs={12}>
                <div style={{ paddingTop: 20 }}>
                  <img src='/images/stat-block-header-bar.svg' alt='' />
                </div>
                <div className={classes.underline}>
                  <Typography className={classes.title} variant='h6'>
                    Equipment
                  </Typography>
                </div>
                <Typography className={classes.titlePad} variant='body1'>
                  No equipment has been purchased
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <div style={{ paddingTop: 20 }}>
                  <img src='/images/stat-block-header-bar.svg' alt='' />
                </div>
                <div className={classes.underline}>
                  <Typography className={classes.title} variant='h6'>
                    Adventure Log
                  </Typography>
                </div>
                <Typography className={classes.titlePad} variant='body1'>
                  No adventures have been logged (yet)
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <div style={{ paddingTop: 20 }}>
                  <img src='/images/stat-block-header-bar.svg' alt='' />
                </div>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <div className={classes.underline}>
                <Typography className={classes.title} variant='h6'>
                  Notes
                </Typography>
              </div>

              <Typography component='div' className={classes.stat} variant='body1'>
                <ReactMarkdown source={character.notes} />
              </Typography>

              <img src='/images/stat-block-header-bar.svg' alt='' />

              <Button
                variant='contained'
                color='primary'
                className={classes.buttonClose}
                size='medium'
                onClick={this.props.handleModalClose}
              >
                <Clear className={classes.iconLeft} />
                Close
              </Button>

              <Button
                variant='contained'
                color='primary'
                className={classes.buttonEdit}
                size='medium'
                onClick={this.props.handleModalClose}
              >
                <Create className={classes.iconLeft} />
                Edit
              </Button>

              {character.link ? (
                <Button
                  variant='contained'
                  color='secondary'
                  className={classes.buttonLink}
                  size='medium'
                  href={character.link}
                  target='_blank'
                  rel='noopener noreferrer'
                >
                  Character
                  <ViewQuilt className={classes.iconRight} />
                </Button>
              ) : (
                <Fragment />
              )}
            </Grid>
          </Grid>
        </Paper>
      </Modal>
    )
  }
}

export default withStyles(styles, { withTheme: true })(CharacterSheet)
