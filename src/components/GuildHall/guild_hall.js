import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Roster from '../Roster'
import Wiki from '../Wiki'

const styles = theme => ({
  paper: {
    flexShrink: 0,
    padding: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit * 2
  },
  title: {
    fontFamily: 'Eczar',
    textAlign: 'center'
  },
  center: {
    textAlign: 'center'
  }
})

class GuildHall extends PureComponent {
  static propTypes = {
    classes: PropTypes.object
  }

  render () {
    const { classes } = this.props

    return (
      <Grid container direction='row' justify='flex-start' alignItems='stretch' alignContent='center'>
        <Grid item md={1} />
        <Grid item xs>
          <Grid container direction='column' justify='flex-start' alignItems='stretch' alignContent='center'>
            <Grid item xs>
              <Paper className={classes.paper}>
                <Wiki id='guild-title' variant='h4' classes={{ wiki: classes.title }} />
                <Wiki id='guild-main' variant='body1' />
              </Paper>
            </Grid>
            <Grid item xs>
              <Roster />
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={1} />
      </Grid>
    )
  }
}

export default withStyles(styles, { withTheme: true })(GuildHall)
