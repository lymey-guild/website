import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Parallax } from 'react-parallax'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import WelcomeCard from '../WelcomeCard'
import Wiki from '../Wiki'

const styles = theme => ({
  paper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit
  },
  paperColumn: {
    backgroundColor: theme.palette.background.article,
    margin: theme.spacing.unit,
    padding: theme.spacing.unit * 2,
    paddingTop: 0
  }
})

class Home extends PureComponent {
  static propTypes = {
    classes: PropTypes.object
  }

  state = {
    width: 0,
    height: 0
  }

  componentDidMount = () => {
    this.updateWindowDimensions()
    window.addEventListener('resize', this.updateWindowDimensions)
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateWindowDimensions)
  }

  updateWindowDimensions = () => {
    this.setState({ width: window.innerWidth, height: window.innerHeight })
  }

  render () {
    const { classes } = this.props
    const screen = {
      width: this.state.width,
      height: this.state.height
    }
    const image = {
      width: 1920,
      height: 1080
    }
    const percent = screen.height / image.height + 0.25

    return (
      <Fragment>
        <Parallax
          style={{ height: screen.height }}
          bgImage='images/lymey_landscape_background.jpg'
          bgWidth={`${image.width * percent}px`}
          bgHeight={`${image.height * percent}px`}
          strength={500}
        >
          <WelcomeCard variant='h6' message='Home of the Lymey Guild' />
        </Parallax>
        <Grid container direction='row' justify='flex-start' alignItems='stretch' alignContent='center'>
          <Grid item md={1} />
          <Grid item xs>
            <Paper className={classes.paper}>
              <Paper className={classes.paperColumn}>
                <Wiki id='home-title' variant='h4' classes={{ wiki: classes.title }} />
                <Wiki id='home-main' variant='body1' />
              </Paper>
            </Paper>
            <Paper className={classes.paper}>
              <Grid container direction='row' justify='flex-start' alignItems='stretch' alignContent='center'>
                <Grid item md={6}>
                  <Paper className={classes.paperColumn}>
                    <Wiki id='home-left-body' />
                  </Paper>
                </Grid>
                <Grid item md={6}>
                  <Paper className={classes.paperColumn}>
                    <Wiki id='home-right-body' />
                  </Paper>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item md={1} />
        </Grid>
      </Fragment>
    )
  }
}

export default withStyles(styles, { withTheme: true })(Home)
