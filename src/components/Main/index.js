import { compose } from 'recompose'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { setAuthorization } from '../../store/actions'
import Main from './main'

export default compose(
  firestoreConnect([
    {
      collection: 'roles'
    }
  ]),
  connect(
    ({ firebase, firestore, user }) => ({
      auth: firebase.auth,
      roles: firestore.data.roles,
      user: user
    }),
    dispatch => ({
      setAuthorization: (user, roles) => dispatch(setAuthorization(user, roles))
    })
  )
)(Main)
