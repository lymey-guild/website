import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { withStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import { hasAccess } from '../../utils'

import GuildHall from '../GuildHall'
import Home from '../Home'
import Notification from '../Notification'
import TopBar from '../TopBar'
import WelcomeCard from '../WelcomeCard'

const styles = theme => ({
  '@global': {
    body: {
      backgroundImage: 'url(images/parchment_paper_wallpaper_texture_seamless.jpg)',
      backgroundRepeat: 'repeat',
      backgroundPosition: 'top left',
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',
      height: '100%'
    }
  },
  root: {
    display: 'flex'
  },
  content: {
    flexGrow: 1
  },
  toolbar: theme.mixins.toolbar
})

const PrivateRoute = ({ access, component: Component, path, user, ...rest }) => {
  const hasAuth = !isEmpty(user) ? hasAccess(user.roles, access) : false

  // if (!hasAuth) {
  //   console.info('Attempt to access restricted route', path, 'by', user) // eslint-disable-line no-console
  // }

  return <Route {...rest} render={props => (hasAuth ? <Component {...props} /> : <Redirect to='/home' />)} />
}

class Main extends PureComponent {
  static propTypes = {
    auth: PropTypes.object,
    classes: PropTypes.object.isRequired,
    user: PropTypes.object,
    roles: PropTypes.array,
    setAuthorization: PropTypes.func.isRequired
  }

  componentDidUpdate () {
    const { auth, roles, user } = this.props

    if (isEmpty(user)) {
      if (isLoaded(auth, roles) && !isEmpty(auth, roles)) {
        this.props.setAuthorization(auth, roles)
      }
    }
  }

  render () {
    const { classes, user } = this.props

    return (
      <Router>
        <div className={classes.root}>
          <CssBaseline />
          <TopBar />
          <Notification />
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <Switch>
              <Route component={Home} path='/home' />
              <Route component={GuildHall} path='/guild' />
              <PrivateRoute access='Admin' component={Home} path='/todo' user={user} />
              <Route
                path='/welcome'
                render={() => <WelcomeCard button message='Coming Soon! Please check back later.' />}
              />
              <Redirect to='/Home' />
            </Switch>
          </main>
        </div>
      </Router>
    )
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(Main)
