import { connect } from 'react-redux'
import { getSelected, setSelected } from '../../store/actions'
import MenuBar from './menu_bar'

export default connect(
  state => ({
    menuItems: state.navMenu.navMenuItems,
    selected: state.navMenu.selected,
    user: state.user
  }),
  dispatch => ({
    setMenu: itemText => dispatch(setSelected(itemText)),
    getSelected: path => dispatch(getSelected(path))
  })
)(MenuBar)
