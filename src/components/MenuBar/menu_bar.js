import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { hasAccess } from '../../utils'
import { Link, withRouter } from 'react-router-dom'

import Button from '@material-ui/core/Button'
import Hidden from '@material-ui/core/Hidden'

const styles = theme => ({
  menuButton: {
    margin: theme.spacing.unit
  },
  toolbarSecondary: {
    backgroundColor: theme.palette.primary.dark,
    justifyContent: 'center'
  }
})

class MenuBar extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    getSelected: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired,
    menuItems: PropTypes.array.isRequired,
    selected: PropTypes.string.isRequired,
    setMenu: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
  }

  componentWillMount () {
    if (!this.props.selected) {
      this.props.getSelected(this.props.location)
    }
  }

  handleNavClick = (e, text) => {
    this.props.setMenu(text)
  }

  render () {
    const { classes, menuItems, selected, user } = this.props

    return (
      <Hidden smDown implementation='js'>
        {(menuItems || [])
          .filter(item => hasAccess(user.roles, item.access))
          .map(item => (
            <Button
              key={item.text}
              component={Link}
              size='small'
              variant={selected === item.text ? 'outlined' : 'text'}
              color='secondary'
              onClick={e => this.handleNavClick(e, item.text)}
              classes={{ root: classes.listItem, selected: classes.selected }}
              className={classes.menuButton}
              to={item.link}
            >
              {item.text}
            </Button>
          ))}
      </Hidden>
    )
  }
}

export default withRouter(withStyles(styles, { withTheme: true })(MenuBar))
