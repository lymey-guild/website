import React, { PureComponent } from 'react'
import { Link, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import { List, ListItem, ListItemText } from '@material-ui/core'
import { hasAccess } from '../../utils'

const styles = theme => ({
  drawer: {
    [theme.breakpoints.up('sm')]: {
      flexShrink: 0,
      width: theme.spacing.background.navWidth
    }
  },
  drawerPaper: {
    width: theme.spacing.background.navWidth,
    boxShadow: 'none',
    border: 'none'
  },
  listItem: {
    margin: 5,
    paddingRight: 0,
    paddingLeft: 0,

    '&$selected': {
      backgroundColor: theme.palette.secondary.dark,
      boxShadow: '1px 1px 2px 0 black inset',
      borderRadius: 5,
      background: `linear-gradient(to right, ${theme.palette.secondary.main}, ${theme.palette.secondary.dark})`,

      '&:hover': {
        backgroundColor: theme.palette.secondary.dark
      }
    },

    '&:hover': {
      backgroundColor: theme.palette.secondary.main
    }
  },
  itemText: {
    fontFamily: 'Rock Salt',
    fontSize: 16,
    textAlign: 'right',
    color: 'inherit'
  },
  toolbar: theme.mixins.toolbar,
  selected: {}
})

class NavDrawer extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    menuItems: PropTypes.array.isRequired,
    selected: PropTypes.string.isRequired,
    setMenu: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
  }

  componentWillMount () {
    if (!this.props.selected) {
      this.props.getSelected(this.props.location)
    }
  }

  handleNavClick = (e, text) => {
    this.props.setMenu(text)
  }

  render () {
    const { classes, menuItems, selected, user } = this.props

    return (
      <nav>
        <Hidden smDown implementation='js'>
          <Drawer
            className={classes.drawer}
            variant='permanent'
            classes={{
              paper: classes.drawerPaper
            }}
          >
            <div className={classes.toolbar} />
            <List>
              {(menuItems || [])
                .filter(item => hasAccess(user.roles, item.access))
                .map(item => (
                  <ListItem
                    button
                    key={item.text}
                    component={Link}
                    selected={selected === item.text}
                    onClick={e => this.handleNavClick(e, item.text)}
                    classes={{
                      root: classes.listItem,
                      selected: classes.selected
                    }}
                    {...{ to: item.link }}
                  >
                    <ListItemText primary={item.text} classes={{ primary: classes.itemText }} />
                  </ListItem>
                ))}
            </List>
          </Drawer>
        </Hidden>
      </nav>
    )
  }
}

export default withRouter(withStyles(styles, { withTheme: true })(NavDrawer))
