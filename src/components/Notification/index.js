import { connect } from 'react-redux'
import { sendNotification, clearNotification } from '../../store/actions'
import Notification from './notification'

export default connect(
  state => ({ messages: state.notifications.messages || [] }),
  dispatch => ({
    sendNotification: message => dispatch(sendNotification(message)),
    clearNotification: () => dispatch(clearNotification())
  })
)(Notification)
