import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Snackbar from '@material-ui/core/Snackbar'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import { isEmpty } from 'react-redux-firebase'

const styles = theme => ({
  close: {
    padding: theme.spacing.unit / 2
  }
})

class Notification extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    messages: PropTypes.array,
    clearNotification: PropTypes.func
  };

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }

    this.props.clearNotification()
  };

  render () {
    const { classes, messages } = this.props
    const message = messages.shift()

    return (
      <Snackbar
        key={message}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        open={!isEmpty(message)}
        autoHideDuration={6000}
        onClose={this.handleClose}
        ContentProps={{ 'aria-describedby': 'message-id' }}
        message={<span id='message-id'>{message}</span>}
        action={[
          <IconButton
            key='close'
            aria-label='Close'
            color='inherit'
            className={classes.close}
            onClick={this.handleClose}
          >
            <CloseIcon />
          </IconButton>
        ]}
      />
    )
  }
}

export default withStyles(styles, { withTheme: true })(Notification)
