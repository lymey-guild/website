import { compose } from 'recompose'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import Roster from './roster'

export default compose(
  firestoreConnect(['characters', 'users']),
  connect(state => ({
    characters: state.firestore.data.characters,
    user: state.user,
    users: state.firestore.data.users
  }))
)(Roster)
