import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { withStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import MUIDataTable from 'mui-datatables'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import withWidth from '@material-ui/core/withWidth'
import { displayUp } from '../../utils'
import CharacterSheet from '../CharacterSheet'
import AddToolbar from './toolbars/add'
import CharToolbar from './toolbars/character'

const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit,
    marginTop: theme.spacing.unit * 3,
    backgroundColor: theme.palette.primary.light
  },
  loading: {
    padding: theme.spacing.unit * 2
  },
  progress: {
    margin: theme.spacing.unit * 2
  },
  subTitle: {
    padding: theme.spacing.unit,
    fontStyle: 'italic'
  }
})

class Roster extends PureComponent {
  static propTypes = {
    characters: PropTypes.object,
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    users: PropTypes.object,
    width: PropTypes.string.isRequired
  }

  state = {
    activeCharacter: {}
  }

  getData = () => {
    const { characters, user, users } = this.props

    return isLoaded(characters) && isLoaded(users)
      ? Object.keys(characters)
        .map(id => {
          const char = characters[id]

          return isEmpty(char)
            ? []
            : [
              id,
              char.name,
              char.level,
              char.race,
              char.class,
              char.sex,
              char.wealth,
              users[char.owner].displayName,
              !isEmpty(user) && (user.isSuperUser || char.owner === user.profile.uid) ? (
                <CharToolbar characterID={id} />
              ) : (
                <Fragment />
              )
            ]
        })
        .sort((a, b) => a[1] - b[1])
      : []
  }

  handleModalClose = () => {
    this.setState({
      activeCharacter: {}
    })
  }

  // function(rowData: string[], rowMeta: { dataIndex: number, rowIndex: number }) => void
  handleRowClick = data => {
    const character = this.props.characters[data[0]]

    this.setState({
      activeCharacter: { ...character, id: data[0], owner: this.props.users[character.owner].displayName }
    })
  }

  render () {
    const { classes, user, width } = this.props
    const data = this.getData()

    const tableOptions = {
      customToolbar: () => (!isEmpty(user) ? <AddToolbar /> : <Fragment />),
      download: !isEmpty(user),
      filter: true,
      filterType: 'dropdown',
      onRowClick: this.handleRowClick,
      pagination: true,
      print: false,
      responsive: 'scroll',
      rowsPerPage: 100,
      selectableRows: false,
      viewColumns: false
    }

    const columns = [
      {
        name: 'ID',
        options: { display: displayUp('none', width), filter: false, sort: false, download: true }
      },
      {
        name: 'Name',
        options: { display: displayUp('all', width), filter: false, sort: true, download: true }
      },
      {
        name: 'Level',
        options: { display: displayUp('all', width), filter: true, sort: true, download: true }
      },
      {
        name: 'Race',
        options: { display: displayUp('sm', width), filter: true, sort: true, download: true }
      },
      {
        name: 'Class',
        options: { display: displayUp('all', width), filter: true, sort: true, download: true }
      },
      {
        name: 'Sex',
        options: { display: displayUp('md', width), filter: true, sort: true, download: true }
      },
      {
        name: 'Wealth',
        options: { display: displayUp('md', width), filter: false, sort: true, download: true }
      },
      {
        name: 'Owner',
        options: { display: displayUp('md', width), filter: true, sort: true, download: true }
      },
      {
        name: null,
        options: { display: displayUp('all', width), filter: false, sort: false, download: false }
      }
    ]

    return (
      <Paper className={classes.root}>
        {!isLoaded(this.props.characters) || !isLoaded(this.props.users) ? (
          <CircularProgress size={24} className={classes.progress} />
        ) : (
          <Fragment>
            <CharacterSheet character={this.state.activeCharacter} handleModalClose={this.handleModalClose} />
            <MUIDataTable title={'Guild Roster'} data={data} columns={columns} options={tableOptions} />
            <Typography className={classes.subTitle} variant='subtitle1'>
              Click on a character for more details
            </Typography>
          </Fragment>
        )}
      </Paper>
    )
  }
}

export default withWidth()(withStyles(styles, { withTheme: true })(Roster))
