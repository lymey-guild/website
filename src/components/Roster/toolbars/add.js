import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import AddIcon from '@material-ui/icons/Add'

const styles = theme => ({
  iconButton: {
    // margin: theme.spacing.unit
  }
})

const handleAddClick = e => {
  e.stopPropagation()
  // console.log('clicked on Add')
}

const AddToolbar = props => {
  const { classes } = props

  return (
    <React.Fragment>
      <Tooltip title={'Add Character'}>
        <IconButton className={classes.iconButton} onClick={handleAddClick}>
          <AddIcon />
        </IconButton>
      </Tooltip>
    </React.Fragment>
  )
}

AddToolbar.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(AddToolbar)
