import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import CreateIcon from '@material-ui/icons/Create'
import DeleteIcon from '@material-ui/icons/Delete'

const styles = theme => ({
  iconButton: {}
})

const handleDeleteClick = (e, char) => {
  e.stopPropagation()
  // console.log('clicked on delete with', char)
}

const handleEditClick = (e, char) => {
  e.stopPropagation()
  // console.log('clicked on edit with', char)
}

const CharToolbar = props => {
  const { classes, characterID } = props

  return (
    <React.Fragment>
      <Tooltip title={'Edit Character'}>
        <IconButton className={classes.iconButton} onClick={e => handleEditClick(e, characterID)}>
          <CreateIcon />
        </IconButton>
      </Tooltip>
      <Tooltip title={'Delete Character'}>
        <IconButton className={classes.iconButton} onClick={e => handleDeleteClick(e, characterID)}>
          <DeleteIcon />
        </IconButton>
      </Tooltip>
    </React.Fragment>
  )
}

CharToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  characterID: PropTypes.string.isRequired
}

export default withStyles(styles, { withTheme: true })(CharToolbar)
