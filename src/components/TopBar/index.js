import { connect } from 'react-redux'
import TopBar from './topbar'
import { setSelected } from '../../store/actions'

export default connect(
  state => ({
    menuItems: state.navMenu.navMenuItems,
    user: {
      auth: state.authorization
    }
  }),
  dispatch => ({ setMenu: itemText => dispatch(setSelected(itemText)) })
)(TopBar)
