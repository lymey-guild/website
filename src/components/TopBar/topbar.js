import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Link, withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Fade from '@material-ui/core/Fade'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import MenuBar from '../MenuBar'
import MenuIcon from '@material-ui/icons/Menu'
import UserMenu from '../UserMenu'
import { hasAccess } from '../../utils'

const styles = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  header: {
    flexGrow: 1,
    fontFamily: 'Eczar',
    [theme.breakpoints.down('xs')]: {
      fontSize: 18
    }
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
})

class TopBar extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    menuItems: PropTypes.array.isRequired,
    setMenu: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
  }

  state = {
    anchorEl: null
  }

  handleMenuClose = (e, text) => {
    this.setState({ anchorEl: null })

    if (this.props.menuItems.map(i => i.text).includes(text)) {
      this.props.setMenu(text)
    }
  }

  handleMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  menuItems = () =>
    this.props.menuItems
      .filter(item => hasAccess(this.props.user.auth, item.access))
      .map(item => (
        <MenuItem
          key={item.text}
          onClick={e => this.handleMenuClose(e, item.text)}
          component={Link}
          {...{ to: item.link }}
        >
          {item.text}
        </MenuItem>
      ))

  menu = () => {
    const anchorEl = this.state.anchorEl

    return (
      <Menu
        id='nav-menu'
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={this.handleMenuClose}
        TransitionComponent={Fade}
      >
        {this.menuItems()}
      </Menu>
    )
  }

  render () {
    const { classes } = this.props

    return (
      <AppBar position='fixed' className={classes.appBar}>
        <Toolbar>
          <Hidden mdUp>
            {this.menu()}
            <IconButton className={classes.menuButton} onClick={this.handleMenuOpen} color='inherit' aria-label='Menu'>
              <MenuIcon />
            </IconButton>
          </Hidden>
          <Typography variant='h4' color='inherit' className={classes.header}>
            Lymey Guild
          </Typography>
          <MenuBar />
          <UserMenu />
        </Toolbar>
      </AppBar>
    )
  }
}

export default withRouter(withStyles(styles, { withTheme: true })(TopBar))
