import { compose } from 'recompose'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import UserMenu from './user_menu'

export default compose(
  firestoreConnect(),
  connect(state => ({
    user: state.user
  }))
)(UserMenu)
