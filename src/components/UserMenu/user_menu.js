import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { isEmpty, isLoaded } from 'react-redux-firebase'
import Avatar from '@material-ui/core/Avatar'
import Chip from '@material-ui/core/Chip'
import Fade from '@material-ui/core/Fade'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import VpnKey from '@material-ui/icons/VpnKey'
import Edit from '@material-ui/icons/Edit'
import { withStyles } from '@material-ui/core/styles'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'

const styles = theme => ({
  icon: {
    margin: theme.spacing.unit,
    fontSize: 12
  },
  userChip: {
    [theme.breakpoints.down('sm')]: {
      paddingRight: -12
    }
  }
})

class UserMenu extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    firebase: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    width: PropTypes.string.isRequired
  }

  state = {
    anchorEl: null
  }

  handleLogin = e => {
    e.preventDefault()

    this.props.firebase
      .login({
        provider: 'google',
        type: 'redirect'
      })
      .catch(error => {
        console.error('Authentication Failed:', JSON.parse(error.message).error) // eslint-disable-line no-console
      })
  }

  handleLogout = e => {
    e.preventDefault()

    this.props.firebase.logout()
    this.handleMenuClose()
  }

  handleMenuClose = () => {
    this.setState({ anchorEl: null })
  }

  handleMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  userMenu = () => {
    const anchorEl = this.state.anchorEl
    const open = Boolean(anchorEl)

    return (
      <Menu
        id='user-menu-fade'
        anchorEl={anchorEl}
        open={open}
        onClose={this.handleMenuClose}
        TransitionComponent={Fade}
      >
        <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
      </Menu>
    )
  }

  render () {
    const { classes, user, width } = this.props

    if (!isLoaded(user)) {
      return <Chip disabled label='Checking Auth' color='secondary' />
    }

    if (isEmpty(user)) {
      return (
        <Chip
          onClick={this.handleLogin}
          clickable
          label='Sign In'
          color='secondary'
        />
      )
    }

    const icon = user.isSuperUser ? (
      <VpnKey className={classes.icon} />
    ) : user.canEditWiki ? (
      <Edit className={classes.icon} />
    ) : (
      <Fragment />
    )

    return (
      <Fragment>
        {this.userMenu()}
        {icon}
        <Chip
          avatar={
            <Avatar
              alt={user.profile.displayName}
              src={user.profile.photoURL}
            />
          }
          clickable
          color='secondary'
          className={classes.userChip}
          label={isWidthUp('sm', width) ? user.profile.displayName : null}
          onClick={this.handleMenuOpen}
        />
      </Fragment>
    )
  }
}

export default withWidth()(withStyles(styles, { withTheme: true })(UserMenu))
