import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Button, Grid, Paper, Typography } from '@material-ui/core'

const styles = theme => ({
  logo: {
    maxWidth: 500,
    width: '80%'
  },
  grid: {
    marginTop: '10%'
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    width: '100%'
  }
})

class Welcome extends Component {
  render () {
    const { classes } = this.props

    return (
      <Grid className={classes.grid} container direction='column' spacing={24} justify='center' alignItems='center'>
        <Grid item xs>
          <Paper className={classes.paper}>
            <img src='/images/lymey-guild-crest.png' alt='LG' className={classes.logo} />
            <Typography gutterBottom variant='h5' component='h2'>
              Welcome to Grand Duchy of Lymey!
            </Typography>
            <Typography variant={this.props.variant} component='p'>
              {this.props.message}
            </Typography>
          </Paper>
        </Grid>
        {this.props.button && (
          <Grid item xs>
            <Button variant='contained' color='primary' target='_blank' href={'https://lymeyguild.tumblr.com/'}>
              But I Want it NOW!
            </Button>
          </Grid>
        )}
      </Grid>
    )
  }
}

Welcome.propTypes = {
  button: PropTypes.bool,
  classes: PropTypes.object.isRequired,
  message: PropTypes.string,
  variant: PropTypes.string
}

export default withStyles(styles, { withTheme: true })(Welcome)
