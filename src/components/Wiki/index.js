import { compose } from 'recompose'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { sendNotification } from '../../store/actions'
import Wiki from './wiki'

export default compose(
  firestoreConnect([
    {
      collection: 'wikis'
    }
  ]),
  connect(
    state => ({
      user: state.user,
      wikis: state.firestore.data.wikis
    }),
    dispatch => ({
      sendNotification: message => dispatch(sendNotification(message))
    })
  )
)(Wiki)
