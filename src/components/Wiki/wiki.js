import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ReactMarkdown from 'react-markdown'
import { isEmpty, isLoaded } from 'react-redux-firebase'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  button: {
    float: 'right',
    margin: theme.spacing.unit
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  progress: {
    margin: theme.spacing.unit * 2
  },
  textField: {
    flexGrow: 1,
    margin: 0,
    border: 0
  },
  wiki: {},
  wikiEdit: {
    cursor: 'text',
    '&:hover': {
      borderLeft: '1px solid',
      borderColor: theme.palette.primary,
      paddingLeft: theme.spacing.unit - 1,
      marginLeft: theme.spacing.unit * -1
    }
  }
})

class Wiki extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    firestore: PropTypes.shape({
      update: PropTypes.func.isRequired
    }).isRequired,
    id: PropTypes.string.isRequired,
    sendNotification: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    variant: PropTypes.string,
    wikis: PropTypes.object
  }

  state = {
    virgin: true,
    modified: false,
    edit: false,
    text: 'Not Loaded'
  }

  componentWillUpdate () {
    if (this.state.virgin && isLoaded(this.props.wikis)) {
      const wiki = this.props.wikis[this.props.id]

      this.setState({
        text: isEmpty(wiki) ? '' : wiki.text,
        virgin: false
      })
    }
  }

  getWiki = () => {
    const wikiObj = {
      text: `Whoops! ${this.props.id || 'This entry'} has not been defined!`,
      type: 'notLoaded',
      label: this.props.id
    }

    return isLoaded(this.props.wikis)
      ? !isEmpty(this.props.wikis[this.props.id])
        ? this.state.virgin
          ? this.props.wikis[this.props.id]
          : { ...this.props.wikis[this.props.id], text: this.state.text }
        : wikiObj
      : {
        ...wikiObj,
        text: (
          <div>
              Loading, please wait...
            <CircularProgress size={24} className={this.props.classes.progress} />
          </div>
        )
      }
  }

  handleChange = event => this.setState({ text: event.target.value, modified: true })

  handleSubmit = e => {
    const { firestore, id, sendNotification, wikis } = this.props
    const wiki = this.getWiki()

    e.preventDefault()

    if (isLoaded(wikis) && !isEmpty(this.state.text) && this.state.modified) {
      firestore
        .update({ collection: 'wikis', doc: id }, { ...wiki, text: this.state.text })
        .then(() => {
          this.setState({ modified: false })
          sendNotification(`${wiki.label || 'This entry'} was updated successfuly`)
        })
        .catch(err => {
          sendNotification(`There was a problem updating ${wiki.label || 'this entry'}: ${err}`)
        })
    }

    this.toggleEdit()
  }

  toggleEdit = () => {
    if (isLoaded(this.props.wikis)) {
      this.setState(state => ({ edit: !state.edit }))
    }
  }

  render () {
    const { user, classes } = this.props
    const wiki = this.getWiki()
    const variant = isEmpty(this.props.variant) ? 'body2' : this.props.variant
    const canEdit = wiki.type !== 'notLoaded' && user.canEditWiki

    return this.state.edit ? (
      <form onSubmit={this.handleSubmit}>
        <TextField
          id={this.props.id}
          variant='outlined'
          multiline={wiki.type !== 'text'}
          fullWidth
          autoFocus
          margin='normal'
          value={this.state.text}
          className={classes.textField}
          onBlur={this.handleSubmit}
          onChange={this.handleChange}
        />
        <Button variant='contained' className={classes.button} type='submit'>
          Done Editing
        </Button>
      </form>
    ) : (
      <Typography
        component='div'
        paragraph={wiki.type === 'markdown'}
        variant={variant}
        className={canEdit ? classes.wikiEdit : classes.wiki}
        onClick={canEdit ? this.toggleEdit : null}
      >
        {wiki.type === 'markdown' ? <ReactMarkdown source={wiki.text} /> : wiki.text}
      </Typography>
    )
  }
}

export default withStyles(styles, { withTheme: true })(Wiki)
