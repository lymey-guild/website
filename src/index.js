import * as serviceWorker from './serviceWorker'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createFirestore } from './store/create_store'
import { MuiThemeProvider } from '@material-ui/core/styles'
import Main from './components/Main'
import theme from './theme'
import { setNavMenu } from './store/actions/nav_menu_action'

const store = createFirestore()
const navMenu = store.getState().navMenu

// Populate navMenu items if not already set
if (!navMenu.navMenuItems) {
  store.dispatch(setNavMenu())
}

// Setup react-redux so that connect HOC can be used
const App = () => (
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <Main />
    </Provider>
  </MuiThemeProvider>
)

render(<App />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
