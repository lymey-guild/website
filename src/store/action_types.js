// Navigation Menu
export const SET_NAV_MENU = 'SET_NAV_MENU'
export const SET_NAV_SELECT = 'SET_NAV_SELECT'
export const GET_NAV_SELECT = 'GET_NAV_SELECT'

// Notifications
export const SEND_NOTIFICATION = 'SEND_NOTIFICATION'
export const CLEAR_NOTIFICATION = 'CLEAR_NOTIFICATION'

// Users
export const GET_USER_AUTH = 'GET_USER_AUTHORIZATION'
export const SET_USER_AUTH = 'SET_USER_AUTHORIZATION'
