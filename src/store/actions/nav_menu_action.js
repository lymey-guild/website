import * as actionTypes from '../action_types'
import navMenuItems from '../menu_items'

export const setNavMenu = () => ({
  type: actionTypes.SET_NAV_MENU,
  payload: {
    menuItems: navMenuItems,
    selected: ''
  }
})

// Obtains menu selection via the Route path
export const getSelected = path => ({
  type: actionTypes.GET_NAV_SELECT,
  payload: path
})

// Set the selected menu item
export const setSelected = selection => ({
  type: actionTypes.SET_NAV_SELECT,
  payload: selection
})
