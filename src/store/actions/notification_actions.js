import * as actionTypes from '../action_types'

export const sendNotification = message => ({
  type: actionTypes.SEND_NOTIFICATION,
  payload: message
})

export const clearNotification = () => ({
  type: actionTypes.CLEAR_NOTIFICATION
})
