import * as actionTypes from '../action_types'

export const setAuthorization = (user, roles) => ({
  type: actionTypes.SET_USER_AUTH,
  payload: { user: user, roles: roles }
})

export const getAuthorization = () => ({
  type: actionTypes.GET_USER_AUTH
})
