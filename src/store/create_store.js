import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore'
import firebase from 'firebase/app'
import thunk from 'redux-thunk'
import makeRootReducer from './root_reducer'
import { applyMiddleware, createStore, compose } from 'redux'
import { getFirebase, reactReduxFirebase } from 'react-redux-firebase'
import { reduxFirestore } from 'redux-firestore'
import { firebase as firebaseConfig } from '../config'
import { version } from '../../package.json'

export const profilePopulates = [{ child: 'role', root: 'roles' }]

export const createFirestore = (initialState = {}) => {
  // ======================================================
  // Window Vars Config
  // ======================================================
  window.version = version

  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [thunk.withExtraArgument(getFirebase)]

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []

  const reduxDevToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__
  if (process.env.NODE_ENV === 'development' && typeof reduxDevToolsExtension === 'function') {
    enhancers.push(reduxDevToolsExtension())
  }

  // react-redux-firebase config
  const reduxConfig = {
    userProfile: 'users',
    enableLogging: false, // enable/disable Firebase Database Logging
    useFirestoreForProfile: true,
    firebaseStateName: 'firebase', // should match the reducer name ("firebase" is default)
    attachAuthIsReady: true // attaches auth is ready promise to store
    // profileParamsToPopulate: profilePopulates, // populates user's role with matching role object from roles
    // profileFactory: user => ({
    // email: user.email || user.providerData[0].email,
    // providerData: user.providerData
    // })
  }

  firebase.initializeApp(firebaseConfig)

  // Initialize Firestore with timeshot settings
  firebase.firestore().settings({ timestampsInSnapshots: true })

  // Create store with reducers and initial state
  const store = createStore(
    makeRootReducer(),
    initialState,
    compose(
      reduxFirestore(firebase),
      reactReduxFirebase(firebase, reduxConfig),
      applyMiddleware(...middleware),
      ...enhancers
    )
  )

  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('./root_reducer', () => {
      const reducers = require('./root_reducer').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}
