export default [
  {
    access: ['All'],
    link: '/home',
    text: 'Home'
  },
  {
    access: ['Admin'],
    link: '/guild',
    text: 'Guild'
  },
  {
    access: ['Admin'],
    link: '/todo',
    text: 'Quests'
  },
  {
    access: ['Admin'],
    link: '/todo',
    text: 'Arcane Emporium'
  }
]
