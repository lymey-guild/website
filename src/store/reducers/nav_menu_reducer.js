import * as actionTypes from '../action_types'

const selectByPath = (menu, path) => {
  return (
    (menu || []).filter(item => {
      return item.link === path.pathname
    })[0] || { text: 'Home' }
  ).text
}

export default function navMenu (state = {}, action) {
  switch (action.type) {
    case actionTypes.SET_NAV_MENU:
      return { ...state, navMenuItems: action.payload.menuItems, selected: action.payload.selected }
    case actionTypes.GET_NAV_SELECT:
      return { ...state, selected: selectByPath(state.navMenuItems, action.payload) }
    case actionTypes.SET_NAV_SELECT:
      return { ...state, selected: action.payload }
    default:
      return state
  }
}
