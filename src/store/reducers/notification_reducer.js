import * as actionTypes from '../action_types'

export default function notifications (state = {}, action) {
  let messages = (state.messages || []).slice(0)

  switch (action.type) {
    case actionTypes.SEND_NOTIFICATION:
      messages.push(action.payload)

      return { messages: messages }
    case actionTypes.CLEAR_NOTIFICATION:
      messages.shift()

      return { messages: messages }
    default:
      return state
  }
}
