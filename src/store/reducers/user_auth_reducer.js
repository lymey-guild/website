import * as actionTypes from '../action_types'
import { constants } from 'react-redux-firebase'
import { getUserRoles } from '../../utils'

const hasPerm = (roles, type) => {
  for (let index in roles) {
    if (roles[index][type]) {
      return true
    }
  }

  return false
}

const getRoles = ({ user, roles }) => {
  const userRoles = getUserRoles(user, roles)

  return {
    profile: user,
    roles: userRoles.map(role => role.name),
    isSuperUser: hasPerm(userRoles, 'superUser'),
    canEditWiki: hasPerm(userRoles, 'wikiEdit')
  }
}

export default function user (state = {}, action) {
  switch (action.type) {
    case constants.actionTypes.AUTH_EMPTY_CHANGE:
      return {}
    case actionTypes.GET_USER_AUTH:
      return { user: state.user }
    case actionTypes.SET_USER_AUTH:
      return { ...getRoles(action.payload) }
    default:
      return state
  }
}
