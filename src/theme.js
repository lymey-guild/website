import { createMuiTheme } from '@material-ui/core/styles'

const themeName = 'Lymey Guild'
const fontFamily = ['Rubik', 'Eczar', 'Rock Salt'].join(',')

const paletteColor = {
  primary: {
    main: '#1B5E20',
    light: '#4C8C4A',
    dark: '#003300',
    contrastText: '#fff'
  },
  secondary: {
    main: '#FFECB3',
    light: '#FFFFE5',
    dark: '#CBBA83',
    contrastText: 'rgba(0, 0, 0, 0.87)'
  },
  dnd: {
    red: '#99291E'
  }
}

const theme = {
  overrides: {
    MUIDataTableToolbar: {
      root: {
        color: paletteColor.primary.dark
      }
    },
    MUIDataTablePagination: {
      root: {
        color: paletteColor.primary.dark
      }
    },
    MUIDataTableBodyCell: {
      cellStacked: {
        backgroundColor: 'inherit !important'
      }
    },
    MUIDataTableHeadCell: {
      fixedHeader: {
        backgroundColor: paletteColor.secondary.dark,
        color: paletteColor.primary.dark,
        fontWeight: 'bold'
      }
    },
    MUIDataTableFilter: {
      root: {
        color: paletteColor.primary.dark
      }
    },
    MuiFormLabel: {
      root: {
        color: 'inherit'
      }
    }
  },
  palette: {
    ...paletteColor,
    background: {
      default: paletteColor.primary.light,
      paper: paletteColor.secondary.light,
      article: '#fefefe'
    },
    text: {
      primary: paletteColor.primary.dark,
      secondary: paletteColor.secondary.light
    }
  },
  spacing: {
    background: {
      navWidth: 240
    }
  },
  typography: {
    color: paletteColor.primary.dark,
    fontFamily: fontFamily,
    useNextVariants: true
  }
}

export default createMuiTheme(theme, themeName)
