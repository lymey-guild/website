export const getUserRoles = (user, roles) => {
  let userId = null
  let userRoles = []

  if (!user) {
    return []
  }

  if ('id' in user) {
    userId = user.id
  }

  if ('uid' in user) {
    userId = user.uid
  }

  if (!userId) {
    return []
  }

  for (let role in roles) {
    const roleObj = roles[role]

    if ('users' in roleObj) {
      if (roleObj.users.includes(userId)) {
        userRoles.push(roleObj)
      }
    }
  }

  return userRoles
}

// Checks if a user has the proper role(s)
export function hasAccess (roles = [], access = []) {
  const accessRoles = [].concat(...Array(access)).map(role => role.toLowerCase())

  if (accessRoles[0] === 'all') {
    return true
  }

  if (accessRoles[0] === 'none') {
    return false
  }

  return roles.filter(role => accessRoles.includes(role.toLowerCase())).length > 0
}
