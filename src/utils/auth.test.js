import { hasAccess, getUserRoles } from '.'

describe('Auth', () => {
  describe('hasAccess', () => {
    it('returns false when not given any parameters', () => {
      expect(hasAccess()).toBe(false)
    })

    it('returns true when access is in roles', () => {
      expect(hasAccess(['Foo', 'Bar', 'Baz'], ['Foo'])).toBe(true)
    })

    it('returns false when access is not in roles', () => {
      expect(hasAccess(['Foo', 'Bar', 'Baz'], ['Bat'])).toBe(false)
    })

    it('returns true when access is "all"', () => {
      expect(hasAccess(['Foo', 'Bar', 'Baz'], ['All'])).toBe(true)
    })

    it('returns false when access is "none"', () => {
      expect(hasAccess(['None', 'Foo', 'Bar', 'Baz'], ['None'])).toBe(false)
    })

    it('returns true when access is in roles but given a String', () => {
      expect(hasAccess(['Foo', 'Bar', 'Baz'], 'Foo')).toBe(true)
    })
  })

  describe('getUserRoles', () => {
    const roles = [{ id: 0, users: ['foo'] }, { id: 1, users: ['bar'] }, { id: 2, users: ['foo', 'bat'] }, { id: 3 }]

    it('returns an Array of roles when given a user object with uid', () => {
      expect(getUserRoles({ uid: 'foo' }, roles)).toEqual([roles[0], roles[2]])
    })

    it('returns an Array of roles when given a user object with id', () => {
      expect(getUserRoles({ id: 'bar' }, roles)).toEqual([roles[1]])
    })

    it('returns an empty Array when no roles match', () => {
      expect(getUserRoles({ id: 'baz' }, roles)).toEqual([])
    })

    it('returns an empty Array when given an invalid user object', () => {
      expect(getUserRoles({ foo: 'bar' }, roles)).toEqual([])
      expect(getUserRoles({}, roles)).toEqual([])
    })

    it('returns an empty Array when given invalid roles', () => {
      expect(getUserRoles({ uid: 'foo' }, [])).toEqual([])
      expect(getUserRoles({ uid: 'foo' }, {})).toEqual([])
    })

    it('handles nulls gracefully', () => {
      expect(getUserRoles(null, roles)).toEqual([])
      expect(getUserRoles({ uid: 'foo' }, null)).toEqual([])
    })
  })
})
