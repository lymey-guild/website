// Checks current media size against a target size
//   Returns true if the current screen is larger than the target
//   Returns false if the current screen is smaller than the target
const _display = (direction, targetWidth, currentWidth) => {
  const widths = ['xs', 'sm', 'md', 'lg', 'xl']
  const current = widths.indexOf(currentWidth)
  const target = widths.indexOf(targetWidth)

  if (targetWidth === 'none') {
    return false
  }

  if (targetWidth === 'all') {
    return true
  }

  if (current < 0 || target < 0) {
    return false
  }

  // 0 == down; 1 == up
  return direction > 0 ? current >= target : current <= target
}

export const displayDown = (targetWidth, currentWidth) => _display(0, targetWidth, currentWidth)
export const displayUp = (targetWidth, currentWidth) => _display(1, targetWidth, currentWidth)
