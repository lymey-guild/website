import { displayUp, displayDown } from '.'

describe('Display', () => {
  describe('displayUp', () => {
    it('returns false for "none"', () => {
      expect(displayUp('none')).toBe(false)
    })

    it('returns true for "all"', () => {
      expect(displayUp('all')).toBe(true)
    })

    it('returns false for widths smaller than current', () => {
      expect(displayUp('lg', 'md')).toBe(false)
    })

    it('returns true for widths larger than current', () => {
      expect(displayUp('md', 'lg')).toBe(true)
    })

    it('returns false for invalid widths', () => {
      expect(displayUp('foo', 'lg')).toBe(false)
      expect(displayUp('xs', 'bar')).toBe(false)
    })
  })
  describe('displayDown', () => {
    it('returns false for "none"', () => {
      expect(displayDown('none')).toBe(false)
    })

    it('returns true for "all"', () => {
      expect(displayDown('all')).toBe(true)
    })

    it('returns false for widths larger than current', () => {
      expect(displayDown('md', 'lg')).toBe(false)
    })

    it('returns true for widths smaller than current', () => {
      expect(displayDown('lg', 'md')).toBe(true)
    })

    it('returns false for invalid widths', () => {
      expect(displayDown('foo', 'lg')).toBe(false)
      expect(displayDown('xs', 'bar')).toBe(false)
    })
  })
})
