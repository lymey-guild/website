export const ABILITY_SCORES = ['STR', 'DEX', 'CON', 'INT', 'WIS', 'CHR']
export const getAbilityIndex = ability => ABILITY_SCORES.indexOf(ability.toUpperCase())

export const calcAbilityBonus = ability => Math.floor((ability - 10) / 2)
export const calcProficiency = level => Math.ceil(level / 4 + 1)

export const calcInitiative = (ability, level, martial = false) =>
  calcAbilityBonus(ability) + (martial ? calcProficiency(level) : 0)
