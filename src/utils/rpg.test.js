import { calcAbilityBonus, calcInitiative, calcProficiency, getAbilityIndex } from '.'

describe('RPG Functions', () => {
  describe('calcAbilityBonus', () => {
    it('returns -5 for 1', () => {
      expect(calcAbilityBonus(1)).toBe(-5)
    })

    it('returns -4 for 2 or 3', () => {
      expect(calcAbilityBonus(2)).toBe(-4)
      expect(calcAbilityBonus(3)).toBe(-4)
    })

    it('returns 0 for 10 or 11', () => {
      expect(calcAbilityBonus(10)).toBe(0)
      expect(calcAbilityBonus(11)).toBe(0)
    })

    it('returns +5 for 20 or 21', () => {
      expect(calcAbilityBonus(20)).toBe(5)
      expect(calcAbilityBonus(21)).toBe(5)
    })

    it('returns +15 for 40 or 41', () => {
      expect(calcAbilityBonus(40)).toBe(15)
      expect(calcAbilityBonus(41)).toBe(15)
    })
  })

  describe('calcInitiative', () => {
    it('returns +2 for a level 3 wizard with a Wisdom of 15', () => {
      expect(calcInitiative(15, 3)).toBe(2)
    })

    it('returns +4 for a level 3 fighter with a Wisdom of 15', () => {
      expect(calcInitiative(15, 3, true)).toBe(4)
    })
  })

  describe('calcProficiency', () => {
    it('returns +2 for level 1', () => {
      expect(calcProficiency(1)).toBe(2)
    })

    it('returns +4 for level 10', () => {
      expect(calcProficiency(10)).toBe(4)
    })

    it('returns +6 for level 20', () => {
      expect(calcProficiency(20)).toBe(6)
    })
  })

  describe('getAbilityIndex', () => {
    it('returns -1 for invalid indecies', () => {
      expect(getAbilityIndex('')).toBe(-1)
    })

    it('returns 0 for STR', () => {
      expect(getAbilityIndex('STR')).toBe(0)
      expect(getAbilityIndex('str')).toBe(0)
    })

    it('returns 1 for DEX', () => {
      expect(getAbilityIndex('DEX')).toBe(1)
      expect(getAbilityIndex('dex')).toBe(1)
    })

    it('returns 2 for CON', () => {
      expect(getAbilityIndex('CON')).toBe(2)
      expect(getAbilityIndex('con')).toBe(2)
    })

    it('returns 3 for INT', () => {
      expect(getAbilityIndex('INT')).toBe(3)
      expect(getAbilityIndex('int')).toBe(3)
    })

    it('returns 4 for WIZ', () => {
      expect(getAbilityIndex('WIS')).toBe(4)
      expect(getAbilityIndex('wis')).toBe(4)
    })

    it('returns 5 for CHR', () => {
      expect(getAbilityIndex('CHR')).toBe(5)
      expect(getAbilityIndex('chr')).toBe(5)
    })
  })
})
